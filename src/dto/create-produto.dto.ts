import { Produto } from './../models/produto.model';

import { IsInt,  IsString } from 'class-validator';

export class CreateProdutoDto extends Produto {
  @IsString()
  codigo: string;
  @IsString()
  nome: string;
  @IsString()
  preco: number;
  @IsInt()
  @IsString()
  categoria_id: number;

}