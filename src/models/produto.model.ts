import { Categoria } from "./categoria.model";

export class Produto{
    id: number;
    codigo:string;
    nome:string;
    preco:number;
    categoria_id:number;
    categoria: Categoria

    constructor(codigo: string, nome: string, preco:number, categoria_id:number){
        this.nome = nome;
        this.codigo = codigo;
        this.preco = preco;
        this.categoria_id = categoria_id;
    }
}