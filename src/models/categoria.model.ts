export class Categoria{
    id: number;   
    nome:string;
    cor:string;  

    constructor(nome: string, cor:string){
        this.nome = nome;
        this.cor = cor;
    }
}