import { CreateProdutoDto } from './../dto/create-produto.dto';
import { PrismaService } from './prisma/prisma.service';
import { Injectable } from '@nestjs/common';
import { Produto } from 'src/models/produto.model';

@Injectable()
export class ProdutoService {
  produtos: Produto[] = [
    new Produto('livro01', ' Meu livro1', 12, 1),
    new Produto('livro02', ' Meu livro1', 15, 1),
    new Produto('livro03', ' Meu livro1', 11, 1),
    new Produto('livro04', ' Meu livro1', 11, 1),
  ];

  constructor(private readonly prisma: PrismaService) {}

  obtemTudo() {
    return this.prisma.produto.findMany();
  }

  obtemUm(id: number): Produto {
    return this.produtos[0];
  }

  criar(data: CreateProdutoDto) {
    /* this.prisma.produto.create({
            da
        });*/
  }

  alterar(produto: Produto): Produto {
    return produto;
  }

  apagar(id: Number) {
    this.produtos.pop();
  }
}
