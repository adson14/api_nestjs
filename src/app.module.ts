import { ProdutoService } from './services/produtos.service';
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ProdutosController } from './controllers/produtos.controller';
import { PrismaService } from './services/prisma/prisma.service';

@Module({
  imports: [],
  controllers: [AppController, ProdutosController],
  providers: [AppService,ProdutoService, PrismaService],
})
export class AppModule {}
