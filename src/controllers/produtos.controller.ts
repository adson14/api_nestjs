import { CreateProdutoDto } from './../dto/create-produto.dto';
import { ProdutoService } from './../services/produtos.service';
import { Body, Controller, Delete, Get, HttpCode, Param, Post, Put } from "@nestjs/common";
import { Produto } from '../models/produto.model'


@Controller('produtos')
export class ProdutosController {

    constructor(private produtoService: ProdutoService){
        
    }

    @Get()
    obtemTodos(){
        return this.produtoService.obtemTudo();
    }
    
    @Get(':id')
    obterUm(@Param() params): Produto{
        return this.produtoService.obtemUm(params.id);
    }

    @Post()
    @HttpCode(204)
    criar(@Body() produto: CreateProdutoDto) {   

        this.produtoService.criar(produto);
    }

    @Put()
    alterar(@Body() produto: Produto): Produto{
        return this.produtoService.alterar(produto);
    }

    @Delete(':id')
    deletar(@Param() params) {
        this.produtoService.apagar(params.id)
    }
}